const setFontSize = (doc) => {
    function setRem () {
        let deviceWidth = doc.documentElement.clientWidth;
        if(deviceWidth > 750) {
            // deviceWidth = 750;
            deviceWidth = 750;
        } else if (!deviceWidth) {
            return;
        }
        window.console.log(deviceWidth/7.5)
        doc.documentElement.style.fontSize = `${deviceWidth / 7.5}px`
    }
    setRem()
    window.addEventListener('orientationchange' in window ? 'orientationchange' : 'resize', setRem, false)
}

export default setFontSize