import Toast from '../components/myAlert'

function showToast() {
    return showToast
}

showToast.install = function (_Vue) {
    // 如果页面已经显示该组件则不可重复调用
    if (document.getElementsByClassName('alertBox').length) {
        window.console.error('想干啥')
        return
    }
    // 创建组件构造器
    let tempToast = _Vue.extend(Toast)
    // 生成组件实例
    let currentConfirm
    function initConfirm() {
        currentConfirm = new tempToast()
        // $mount挂载组件通过$el拿到组件dom元素
        let tpl = currentConfirm.$mount().$el
        // 将组件dom元素插入body
        document.body.appendChild(tpl)
    }


    // 在_Vue 原型上添加对象，在组件中可以通过this.$toast访问
    _Vue.prototype.$toast = {
        // 控制toast显示的方法
        show(toastOptions) {
            if(!currentConfirm) initConfirm()
            // 如过传入的是字符串则，显示组件并以该字符串为组件中的text属性其余参数默认
            if (typeof toastOptions === 'string') {
                currentConfirm.text = toastOptions
            }
            // 如果是对象则与实例对象合并
            else if (typeof toastOptions === 'object') {
                Object.assign(currentConfirm, toastOptions)
            }
            currentConfirm.show = true
            setTimeout(() => {
                this.hide()
            }, currentConfirm.time)
        },
        // 控制toast消失的方法
        hide() {
            setTimeout(() => {
                document.body.removeChild(currentConfirm.$el)
                currentConfirm.$destroy()
                currentConfirm = null
            })
        }
    }
}

export default showToast
