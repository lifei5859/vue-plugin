import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const Index = () => import('@/pages/index')
const UploadImg = () => import('@/pages/uploadImg')
const ShowToast = () => import('@/pages/showToast')
const MyConfirm = () => import('@/pages/showConfirm')
const Admin = () => import('@/pages/admin')
const SoundCodeShow = () => import('@/pages/soundCodeShow')
const ShowSwiper = () => import('@/pages/showSwiper')
const ShowMyForm = () => import('@/pages/showMyForm')

let router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Index
        },
        {
            path: '/upload',
            component: UploadImg
        },
        {
            path: '/toast',
            component: ShowToast
        },
        {
            path: '/confirm',
            component: MyConfirm
        },
        {
            path: '/admin',
            component: Admin
        },
        {
            path: '/soundCodeShow/:id',
            component: SoundCodeShow
        },
        {
            path: '/swiper',
            component: ShowSwiper
        },
        {
            path: '/showForm',
            component: ShowMyForm
        }
    ]
})

export default router
